Greatest common divisor of n.

PSEUDO Code
```
function gcd(a, b)
  if a modulo b is 0
    return b
  else
    return gcd(b, a modulo b)
```
