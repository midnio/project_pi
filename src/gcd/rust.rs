fn gcd(a: i32, b: i32) -> i32 {
    if a % b == 0 {
        b
    }
    else {
        gcd(b, a % b)
    }
}
