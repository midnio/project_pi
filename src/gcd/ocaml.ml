let rec gcd a b =
  if (a mod b) = 0 then 1
  else gcd b (a mod b)
