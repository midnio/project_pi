Create a function to find factorial of n.

PSEUDO Code
```
function factorial(n)
  if n is 1
    return 1
  else
    return n * factorial (n - 1)
```
