(* Solution 1 *)
let rec factorial n =
  if n = 1 then
    1
  else
    n * factorial(n - 1)

(* Solution 2 *)
let rec factorial = function
| 0 -> 1
| n -> n * factorial(n - 1)
