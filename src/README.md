# Source
Directory system is like this.
For example
```
src
 | README.md -> This file
 | factorial
      | README.md -> Rules
      | python
      | ocaml
      | rust
      | ...
 | fibonacci
      | README.md -> Rules
      | elixir
      | ocaml
      | ruby
      | ...
 | ...
```

And will be continue like this.
To create a new problem, you should
create a new directory, add README.md
and add a solution in any language.
For multiple solutions add a comment
to head of second/number of solution.
For example with python code
```python
# Solution 1
def a(n): return n * n

# Solution 2
a = lambda n: n * n
```
Also, don't use any library to solve
any problem in any language. Thanks!
