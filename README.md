# Project PI
Project PI is a collection of many problem solutions in many programming languages.

To contribute project:
* Add a not existing solution for a programming language.
* Commit messages should be like `Add PROGRAMMING_LANGUAGE solution for PROBLEM_NAME`.
* Create a new problem and add a solution for it in any programming language.

What is not allowed:
* Removing whitespaces
* Removing/adding a charatcher (or a useless string)
* Adding code/text not related to the problem.

If you want to do something new, please contact with owner.
